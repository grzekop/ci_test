<?php

class Unit extends CI_Controller {

    public function index() {
        /*
         * This breaks normal CodeIgniter convention.
         * We need to load the views first before we can
         * execute the unit testing.
         */

        $this->load->library('unit_test');

        $this->load->model('unit_model');
        $tests = $this->unit_model->retrieve_tests();

        foreach ($tests as $test)
            $this->unit->run($test['rv'], $test['ev'], $test['t'], $test['n']);

        $data['tests'] = $this->unit->result();
        $data['count'] = count($data['tests']);
        $data['failed'] = $this->unit_model->count_failed_tests($data['tests']);

        $this->load->view('include/header');
        $this->load->view('templates/unit', $data);
        $this->load->view('include/footer');
    }

    public function save() {
        foreach (array('video', 'audio') as $type) {
            if (isset($_FILES["${type}-blob"])) {

                $fileName = $_POST["${type}-filename"];
                $uploadDirectory = './assets/uploads/' . $fileName;

                if (!move_uploaded_file($_FILES["${type}-blob"]["tmp_name"], $uploadDirectory)) {
                    echo(" problem moving uploaded file");
                }

                echo($uploadDirectory);
            }
        }
    }

}
